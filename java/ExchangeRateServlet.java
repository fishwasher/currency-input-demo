import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ExchangeRateServlet extends HttpServlet {

   public void init() throws ServletException {
      // do nothing
   }

   public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

      double rate = 2.0 + Math.random();

      response.setContentType("text/plain");

      PrintWriter out = response.getWriter();
      out.println(rate);
   }

   public void destroy() {
      // do nothing.
   }
}
