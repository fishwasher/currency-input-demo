import PropTypes from 'prop-types';
import React, {Component} from 'react';
import axios from 'axios';
import {toFloat, formatAmount} from './util';
import CurrencyInput from './CurrencyInput';

const serviceUrl = '/api/xrate';

async function getRate() {
  const res = await axios.get(serviceUrl + '?ts=' + Date.now())
    .then(res => {
      return toFloat(res.data);
    })
    .catch(function (error) {
      console.log(error);
      return 0;
    });
    return res;
}

export default class CurrencyConverter extends Component {

  constructor(props) {
    super(props);
    this.state = {
      rate: 0
    }
    this.updateRate = this.updateRate.bind(this);
  }
  async updateRate() {
    const delta = Number.parseInt(this.props.interval) || 0;
    const rate = await getRate();
    this.setState({rate});
    if (delta > 0) {
      window.setTimeout(this.updateRate, delta);
    }
  }

  componentDidMount() {
    this.updateRate();
  }

  handleInputChange(data) {
    let {amount, currency} = data;
    this.amount = toFloat(amount);
    this.setState({currency});
  }

  getConvertedStr() {
    const value = this.state.rate * this.props.amount;
    return value ? formatAmount(value) : '';
  }

  render() {
    const converted = this.state.rate * this.state.amount;
    return [
      <div className="converted">
        <div className="amount">USD <strong>${this.getConvertedStr()}</strong></div>
        <div className="rate">Rate: 1 USD = {this.state.rate && this.state.rate.toFixed(4) || ''} {this.props.currency}</div>
      </div>
    ];
  }
}

CurrencyConverter.propTypes = {
  interval: PropTypes.number,
  amount: PropTypes.number,
  currency: PropTypes.string
}
