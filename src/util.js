export const currencies = ['CAD', 'GBP', 'EUR'];

export const toFloat = value => Number.parseFloat(value) || 0;

export const formatAmount = value => toFloat(value).toFixed(2);
