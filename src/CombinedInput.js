import React, {Component} from 'react';
import {currencies, toFloat} from './util';
import CurrencyInput from './CurrencyInput';
import CurrencyConverter from './CurrencyConverter';


export default class CombinedInput extends Component {

  constructor(props) {
    super(props);
    this.id = Math.random().toString(36).substring(2);
    this.state = {
      currency: currencies[0],
      amount: 0
    }
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(data) {
    let {amount, currency} = data;
    amount = toFloat(amount);
    this.setState({currency, amount});
  }

  render() {
    return [
      <div className="combined">
        <CurrencyInput callback={this.handleInputChange} currency={this.state.currency} />
        <CurrencyConverter currency={this.state.currency} amount={this.state.amount} {...this.props} />
      </div>
    ];
  }
}
