import React from 'react';
import ReactDOM from 'react-dom';
import CurrencyInput from './CurrencyInput';
import CombinedInput from './CombinedInput';


function renderPage(props) {
  return [
    <section>
      <h2>Part 1</h2>
      <CurrencyInput />
    </section>,
    <section>
      <h2>Part 2</h2>
      <CombinedInput />
    </section>,
    <section>
      <h2>Part3</h2>
      <CombinedInput interval={100} />
    </section>
  ];
}

ReactDOM.render(renderPage(), document.getElementById('app-target'));
