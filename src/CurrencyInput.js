import PropTypes from 'prop-types';
import React, {Component} from 'react';

import {currencies, toFloat} from './util';

const floatChars = value => {
  let val = ('' + value)
    .replace(/[^\d.]+/g, '')
    .split('.')
    .slice(0,2)
    .join('.')
    ;
  let pos = val.indexOf('.');
  return ~pos ? val.substring(0, pos + 3) : val;
}

export default class CurrencyInput extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currency: this.props.currency || currencies[0],
      amount: 0
    }
    this.handleAmountChange = this.handleAmountChange.bind(this);
    this.handleAmountReset = this.handleAmountReset.bind(this);
    this.handleCurrencyChange = this.handleCurrencyChange.bind(this);
    this.notifyParent = this.notifyParent.bind(this);
  }

  notifyParent(state) {
    if (typeof this.props.callback === 'function') {
      this.props.callback(state);
    }
  }

  handleAmountReset() {
    const amount = 0;
    this.setState({amount});
    this.notifyParent(Object.assign({}, this.state, {amount}));
  }

  handleCurrencyChange(event) {
    const currency = event.target.value;
    this.setState({currency});
    this.notifyParent(Object.assign({}, this.state, {currency}));
  }

  handleAmountChange(event) {
    const amount = floatChars(event.target.value);
    this.setState({amount});
    this.notifyParent(Object.assign({}, this.state, {amount}));
  }

  render() {
    return (
      <fieldset>
        <label for="currency"><span>Currency:</span><select name="currency" onChange={this.handleCurrencyChange}>
        {
          currencies.map((cur, idx) => {
            return (
              <option key={idx} value={cur} selected={cur === this.state.currency}>{cur}</option>
            );
          })
        }
        </select>
        </label>
        <label for="amount"><span>Amount:</span>
          <input type="text" name="amount" value={this.state.amount || ''} onChange={this.handleAmountChange} />
          <button className="small-screen-only" onClick={this.handleAmountReset}></button>
        </label>
      </fieldset>
    );
  }
}

CurrencyInput.propTypes = {
  currency: PropTypes.string,
  callback: PropTypes.func
}
