FROM tomcat:9-jre8

# Define Tomcat directories
ENV WEBAPPSDIR /usr/local/tomcat/webapps
ENV APPDIR $WEBAPPSDIR/api
ENV WEBINFDIR $APPDIR/WEB-INF
ENV CLASSDIR $WEBINFDIR/classes
ENV DOCROOT $WEBAPPSDIR/ROOT

# create app directories
RUN mkdir $APPDIR
RUN mkdir $WEBINFDIR
RUN mkdir $CLASSDIR

# remove Tomcat default home page that shadows index.html
RUN rm $DOCROOT/index.jsp

# copy locally prepared stuff to Tomcat dirs
COPY ./java/web.xml $WEBINFDIR
COPY ./java/ExchangeRateServlet.class $CLASSDIR
COPY ./demo $DOCROOT

EXPOSE 8080
