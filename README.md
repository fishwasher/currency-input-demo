# Currency input demo

## Project details

- Part 1: Reusable currency input component.

- Part 2. Currency converter component reusing the input above. Exchange rate is provided via Ajax service as a random number `2.0 <= rate < 3.0`. The rate is provided once when the component gets loaded.

- Part 3. Same as the component in Part 2, but the rate gets updated every 100ms.

## Implementation

- Client-side app that makes use of ReactJS library;

- Source code is written in JS+JSX, transpiled with Babel and bundled using Webpack;

## Deployment options

Clone the repo locally, open up a terminal (or CMD/PS) window, `cd` into the project directory.

### Running locally with Node (back-end: JavaScript/Express)

Requirements: Node v.10+ installed on the local machine.

Install dependencies:

        npm install

Build client library:

        npm run build

Start Express HTTP server:

        npm start

Open up a web browser, find the app running at `http://localhost:8001`

To close the app hit `Ctrl-C`

### Running locally in Docker container (back-end: Java/Tomcat)

Requirements: Docker must be installed on local machine

Pre-compiled client JavaScript library and Java servlet are used for Docker deployment.

JavaScript library file is built with Webpack in the same fashion as for the Node deployment above.

Java servlet is compiled using local Java 8 SDK, plus `javax.servlet` dependencies borrowed from Tomcat 9 distribution bundle.

Docker image: [tomcat:9-jre8](https://github.com/docker-library/tomcat)

To start the container, run

        docker-compose up

Open up a web browser, find the app running at `http://localhost:8000`

When finished, hit `Ctrl-C`. Then, to destroy the container, run

        docker-compose down
