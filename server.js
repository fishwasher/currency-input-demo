const
  express = require("express"),
  morgan = require("morgan"),
  path = require("path"),
  http = require('http'),
  fs = require('fs'),
  app = express(),
  port = process.env.PORT || 8001;
//
app.use(morgan("dev"));

const staticDir = path.resolve(__dirname, 'demo');

const getRate = () => '' + (2 + Math.random());

app.get('/', function(req, res) {
  res.set({
    'Content-Type':'text/html'
  });
  fs.readFile(path.resolve(staticDir, 'index.html'),
    (err, stuff) => {
      if (err) throw err;
      res.send(stuff);
    });
});

app.get('/api/xrate', function(req, res) {
  res.set({
    'Content-Type':'text/plain'
  });
  res.send(getRate());
});

app.use(express.static(path.resolve(__dirname, 'demo')));

http.createServer(app).listen(port, function () {
    console.log( "Express server listening on port " + port);
});
